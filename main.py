from metier.model import Model
from metier.service import Service
from metier.server import Serveur
import csv

def getModel():
    with open('servers_catalog.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        model = []
        for row in spamreader:
            model.append(Model(row[0], int(row[1]), int(row[2]), int(row[3]), int(row[4]), int(row[5])))
        return model

def getService(file):
    with open(file, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        service = []
        for row in spamreader:
            service.append(Service(row[0], int(row[1]), int(row[2]), int(row[3])))
        return service

def whriteInCsvServer(file, listeServer):
    fichier = open(file, "w")
    for unServeur in listeServer:
        uneLigne = unServeur.getModel().getModel() + ","
        for i in range(0, len(unServeur.getListeServices())):
            if i != len(unServeur.getListeServices())-1:
                uneLigne = uneLigne + unServeur.getListeServices()[i] + ","
            else:
                uneLigne = uneLigne + unServeur.getListeServices()[i].getId()
        fichier.write(uneLigne + "\n")

# input 1 = 6


def coutTotal(listeServ,nbAnnee):
    ctt=0
    for unServ in listeServ:
        ctt+=unServ.cout(nbAnnee)
    return ctt

if __name__ == '__main__':
    lesModel = getModel()
    lesServiceInput1 = getService("inputs/ctstfr0280_input_1.csv")

    listeServ = [Serveur(lesModel[0]), Serveur(lesModel[1])]
    print(coutTotal(listeServ,1))