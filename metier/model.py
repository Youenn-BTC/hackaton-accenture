from typing import List
# from service import Service

class Model:

    def __init__(self, model, co2Production, co2Usage, disk, ram, cores):
        self.__model = model
        self.__co2Production = co2Production
        self.__co2usage = co2Usage
        self.__disk = disk
        self.__ram = ram
        self.__cores = cores
        self.__service = []


    def getModel(self):
        """permet de recuperer l'attribut __model
        :return:
        """
        return self.__model

    def getCo2Production(self):
        """permet de recuperer l'attribut __co2Production
        :return:
        """
        return self.__co2Production

    def getCo2Usage(self):
        """permet de recuperer l'attribut __co2usage
        :return:
        """
        return self.__co2usage

    def getDisk(self):
        """permet de recuperer l'attribut __disk
        :return:
        """
        return self.__disk

    def getRam(self):
        """permet de recuperer l'attribut __ram
        :return:
        """
        return self.__ram

    def getCores(self):
        """permet de recuperer l'attribut __cores
        :return:
        """
        return self.__cores

    def getService(self):
        """permet de recuperer l'attribut
        :return:
        """
        return self.__service

    def addService(self, unService):
        self.__service.append(unService)
