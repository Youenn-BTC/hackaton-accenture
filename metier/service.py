class Service:
    def __init__(self, id: str, stockage: int, ram: int, proc: int):
        self.__proc = proc
        self.__stockage = stockage
        self.__ram = ram
        self.__id = id

    def getId(self) -> str:
        """permet de recuperer l'attribut __id
        :return:
        """
        return self.__id

    def getProc(self) -> int:
        return self.__proc

    def getRam(self) -> int:
        return self.__ram

    def getStock(self) -> int:
        return self.__stockage