
class Serveur:

    def __init__(self, modele):
        self.modele = modele
        self.listeService = []

    def addService(self, service):
        disk = service.getStock()
        ram = service.getRam()
        proc = service.getProc()

        diskDisp = disk <= self.getDiskDispo()
        ramDisp = ram <= self.getRamDispo()
        procDisp = proc <= self.getProcDispo()

        if diskDisp and ramDisp and procDisp:
            self.listeService.append(service)
            return True
        else:
            return False

    def getModel(self):
        return self.modele

    def getListeServices(self):
        return self.listeService


    def delService(self, service):
        self.listeService.remove(service)



    def getDiskDispo(self):
        diskMax = self.modele.getDisk()

        utile = 0
        for serv in self.listeService:
            utile += serv.getStock()

        return diskMax - utile

    def getRamDispo(self):
        ramMax = self.modele.getRam()

        utile = 0
        for serv in self.listeService:
            utile += serv.getStock()

        return ramMax - utile

    def getProcDispo(self):
        procMax = self.modele.getProc()

        utile = 0
        for serv in self.listeService:
            utile += serv.getProc()

        return procMax - utile

    def cout(self,nbAnnee: int)->float:
        """cout
        :param nbAnnee->int:
        :return int:
        """
        return self.modele.getCo2Production()+nbAnnee*self.modele.getCo2Usage()
